• text (string): Prints a given text including the GMR prefix
Print(text)

• text (string): Displays a given text in GMR_LOG
Log(text)

• folders (table): Contains all profile folders
folders = GetProfileFolders()

• folder (string): The profile folder
• profiles (table): Contains all profiles of a given profile folder 
profiles = GetFolderProfiles(folder)

• object1 (Unlocker specific): First object/unit
• object2 (Unlocker specific): Second object/unit
• distance (number): The distance between object1 and object2
distance = GetDistanceBetweenObjects(object1, object2)

• x1, y1, z1 (number): First position 
• x2, y2, z2 (number): Second position
• distance (number): The distance between first and second position
distance = GetDistanceBetweenPositions(x1, y1, z1, x2, y2, z2)

• object1 (Unlocker specific): First object/unit
• object2 (Unlocker specific): Second object/unit
• angles (number): The angles between object1 and object2
angles = GetAnglesBetweenObjects(object1, object2)

• x1, y1, z1 (number): First position 
• x2, y2, z2 (number): Second position
• angles (number): The angles between first and second position
angles = GetAnglesBetweenPositions(x1, y1, z1, x2, y2, z2)

• x1, y1, z1 (number): First position 
• x2, y2, z2 (number): Second position
• distance (number): The distance between x, y, z and x1, y1, z1
• x, y, z (number): The new position
x, y, z = GetPositionBetweenPositions(x1, y1, z1, x2, y2, z2, distance)

• x, y, z (number): The position to check
• radius (number): Max allowed threshold
• isOrNot (boolean): Whether you are at x, y, z or not
isOrNot = IsPlayerPosition(x, y, z, radius)

• isOrNot (boolean): Whether GMR is currently executing or not
isOrNot = IsExecuting()

• GMR starts executing
Execute()

• GMR stops executing
Stop()

• Resets loaded profile data
ResetProfile()

• profileName (string): The currently loaded profiles name
profileName = GetProfileName()

• profileType (string): The currently loaded profiles type
profileType = GetProfileType()

• profileFolder (string): The folder that contains the profile you want to load
• profileName (string): The profile you want to load
LoadProfile(profileFolder, profileName)

• Defines a quest enemy mob
• id (number): The enemy mobs id
DefineQuestEnemyId(id)

• Defines a profile enemy mob
• id (number): The enemy mobs id
DefineProfileEnemyId(id)

• Defines a profile name
• name (string): The profiles name
DefineProfileName(name)

• profileName (string): The name of the next profile
• profileType (string): The type of the next profile (Grinding, Herbalism, Mining, Skinning, Fishing, Minutes)
• value (number): Threshold
• Defines the next profile being loaded automatically under certain conditions
• Example: DefineNextProfile("Northshire 3-6", "Grinding", 3)
DefineNextProfile(profileName, profileType, value)

• profileType (string): The profiles type (Grinding, Gathering, Dungeon, ..)
• Defines a profile type 
DefineProfileType(profileType)

• continent (string): The profiles continent (Kalimdor, Eastern Kingdoms, Outland, ...)
DefineProfileContinent(continent)

• x, y, z (number): Defines a mailbox for a profile at given coordinates 
DefineProfileMailbox(x, y, z)

• x, y, z (number): Defines a center for a profile at given coordinates
• radius (number): Radius around x, y, z 
DefineProfileCenter(x, y, z, radius)

• x, y, z (number): Innkeeper position 
• id (number): Innkeeper ID
• Defines an Innkeeper for a profile
DefineHearthstoneBindLocation(x, y, z, id)

• x, y, z (number): Vendor position 
• id (number): Vendor ID 
• Defines a sell vendor for a profile
DefineSellVendor(x, y, z, id)

• x, y, z (number): Vendor position 
• id (number): Vendor ID 
• Defines a repair vendor for a profile
DefineRepairVendor(x, y, z, id)

• x, y, z (number): Vendor position 
• id (number): Vendor ID 
• Defines a goods vendor for a profile
DefineGoodsVendor(x, y, z, id)

• x, y, z (number): Vendor position 
• id (number): Vendor ID 
• Defines an ammo vendor for a profile
DefineAmmoVendor(x, y, z, id)

• vendorType (string): The vendor type (Goods, Sell, Repair, Ammo)
• x, y, z (number): Position of a waypoint on the vendor path
• Defines a hardcoded path to a vendor
DefineVendorPath(vendorType, x, y, z)

• x, y, z (number): Position of a waypoint on the mailbox path
• Defines a hardcoded path to a mailbox
DefineMailboxPath(x, y, z)

• x, y, z (number): The blacklists center 
• radius (number): The blacklists radius in yards
• Defines a blacklist to avoid objects at a given position within a given radius
DefineAreaBlacklist(x, y, z, radius)

• x, y, z (number): The blacklists center 
• radius (number): The blacklists radius in yards
• timer (number): The blacklists expiration in seconds
• Defines a blacklist to avoid objects at a given position within a given radius for a given time
• Example: GMR.DefineTempAreaBlacklist(1, 2, 3, 10, GetTime()+60)
DefineTempAreaBlacklist(x, y, z, radius, timer)

• id (number): The objects ID 
• Defines a custom object to collect 
DefineCustomObjectId(id)

• itemId (number): The items ID
• isOrNot = Wether an item with itemId as ID is existant either in your bags or equipped
isOrNot = ItemExists(itemId)

• itemId (number): The items ID 
• isOrNot = Wether an item with itemId as ID exists in your bags or not
isOrNot = IsItemInBags(itemId)

• info (table): Contains filters to find specific objects 
• info.id (number): The objects id 
• info.creatureTypeId (number): The objects createtype ID
• info.rawType (number): The objects rawtype
• info.position (table): The objects position
• info.health (number): The objects health (requires info.healthVar)
• info.healthVar (string): "<" or ">" depending if the objects health is > info.health or < info.health 
• info.isAlive (bool): Whether the object is alive or not 
• info.inCombat (bool): Wether the object is in combat or not 
• info.movementFlag (number): The object must have number as movement flag
• info.dynamicFlag (number): The object must have number as dynamic flag
• info.flag (number): The object must have number as flag
• info.flag2 (number): The object must have number as flag2
• info.killedByPlayer (bool): The object must be killed by the player
• info.speed (number): The objects speed must be equal to number
• info.canAttack (bool): Wether the object is attackable or not
• info.hasBuff (string): The object must have a given buff
• info.hasDebuff (string): The object must have a given debuff
• info.notHasBuff (string): The object must not have a given buff
• info.notHasDebuff (string): The object must not have a given debuff
• info.distance (number): The object must be within a given distance
• info.isInteractable (bool): Wether the object is interactable or not
• info.hasGossip (bool): Wether the object has a gossip or not
• info.isPickPocketable (bool): Wether the object is pickpocketable or not
• Example: object = GetObjectWithInfo({ id = 123, rawType = 8, isAlive = true, inCombat = true, speed = 3 })
GetObjectWithInfo(info)

• object (Unlocker specific): The object you want to check 
• range = GetAggroRange(object)
GetAggroRange(object)

• x, y, z (number): The position
• isOrNot = Wether a given position is underwater or not
isOrNot = IsPointUnderwater(x, y, z)

• x, y, z (number): The position
• isOrNot = Wether a given position is in the air or not
isOrNot = IsPointInTheAir(x, y, z)

• Stops GMR's navigation
StopMoving()

• unit (string): A unit token
• isOrNot = Wether a given unit is dead or not
isOrNot = IsDead(unit)

• unit (string): A unit token
• isOrNot = Wether a given unit is ghost or not
isOrNot = IsGhost(unit)

• unit (string): A unit token
• isOrNot = Wether a given unit is alive or not
isOrNot = IsAlive(unit)

• unit (string): A unit token
• isBeingTargeted (bool): Wether the unit is being targeted or not
• Wether a given unit is in combat or not [and being targeted or not]
isOrNot = InCombat(unit [, isBeingTargeted])

• id (number): The objects ID you want to blacklist 
BlacklistId(id)

• guid (string): The objects GUID you want to blacklist
BlacklistGUID(guid)

• Resets defined objects (Lootables, Nodes, Skinnables, ..)
ResetSetObject()

• enemy (Unlocker specific): The object
enemy = GetPlayerAttackingEnemy()

• numSurroundingEnemies (number): The number of scanned enemies around player
numSurroundingEnemies = GetNumSurroundingEnemies()

• enemy (Unlocker specific): The closest object attacking the player
enemy = GetAttackingEnemy()

• unit (string): A unit token 
• buff (string): Checked buff on unit 
• byPlayer (bool): Wether the buff was casted by player or not
HasBuff(unit, buff [, byPlayer])

• unit (string): A unit token 
• debuff (string): Checked debuff on unit 
HasDebuff(unit, debuff)

• unit (string): A unit token 
• isOrNot (bool): Wether given unit is eating or not
isOrNoth = IsEating(unit)

• unit (string): A unit token 
• isOrNot (bool): Wether given unit is drinking or not
isOrNot = IsDrinking(unit)

• itemName (string): The items name 
• isEating (bool): Wether player is currently eating or not 
• itemCount (number): The item count
• containerSlot (number): The container slot 
• bagSlot (number): The bag slot 
itemName, isEating, itemCount, containerSlot, bagSlot = GetFood()

• isOrNot (bool): Wether the item is eatable or not 
isOrNot = IsEatable(item)


• itemName (string): The items name 
• isDrinking (bool): Wether player is currently drinking or not 
• itemCount (number): The item count
• containerSlot (number): The container slot 
• bagSlot (number): The bag slot 
itemName, isDrinking, itemCount, containerSlot, bagSlot = GetDrink()

• isOrNot (bool): Wether the item is drinkable or not 
isOrNot = IsDrinkable(item)

• index (number): The currently active center index
index = GetCentralIndex()

• index (number): The center index 
• Defines your current center index
SetCentralIndex(index)

• enemy (object): The closest object attacking player
enemy = GetPlayerAttackingPlayer()

• unit (string): A unit token
• isOrNot (bool): Wether unit was killed by player or not
isOrNot = IsUnitKilledByPlayer(unit)

• x, y, z (number): The position
• isOrNot (bool): Wether the position is blacklisted or not
isOrNot = IsBlacklistedArea(x, y, z)

• x, y, z (number): The position
• isOrNot (bool): Wether the position is temporary blacklisted or not
isOrNot = IsTempBlacklistedArea(x, y, z)

• x, y, z (number): The position
• Meshes to a given position
MeshTo(x, y, z)

• unit (string): A unit token 
• distance (number): Max allowed distance between unit and nearby enemies to count
• count (number): The number of nearby enemies
count = GetNumEnemies(unitOrObject, distance)

• unit (string): A unit token 
• distance (number): Max allowed distance between unit and nearby attacking enemies to count
• count (number): The number of nearby attacking enemies
count = GetNumAttackingEnemies(unit, distance)

• unit (string): A unit token
• timeInSec (number): Time in seconds
• isOrNot (bool): Wether units cast is < than timeInSec
isOrNot = UnitCastingTime(unit, timeInSec)

• unit (string): A unit token 
• isOrNot (bool): Wether unit is interruptable or not 
isOrNot = IsInterruptable(unit)

• spell (string): The spell to cast
• unit (string): A unit token 
• Casts a spell at unit
Cast(spell, unit)

• unit (string): A unit token 
• health (number): Units health in percent
health = GetHealth(unit)

• unit (string): A unit token 
• mana (number): Units mana in percent
mana = GetMana(unit)

• unit (string): A unit token 
• rage (number): Units rage
rage = GetRage(unit)

• unit (string): A unit token
• otherUnit (string): A unit token
• isOrNot (bool): Wether unit and otherUnit are in line of sight
isOrNot = InLoS(unit, otherUnit)

• x1, y1, z1 (number): First position 
• x2, y2, z2 (number): Second position
• isOrNot (bool): Wether first and second position are in line of sight 
isOrNot = ArePositionsInLoS(x1, y1, z1, x2, y2, z2)

• x, y, z (number): A position 
• groundZ (number): Terrain z coordinate
groundZ = GetGroundZ(x, y, z)

IsSpellKnown(spell)

IsCastable(spell, unit, range, otherUnit, ignoreMana, ignoreCooldown, hasItem)

IsShapeshiftedCastable(spell, unit)

IsWarriorCastable(spell, unit)

CancelBuff(buff)

IsAoECastable(spell, unit)

CastAoE(spell, unit)

IsCheckedItem(item)

IsCheckedSpell(spell)

GetPowerFilter(spell)

GetManaFilter(spell)

GetStackFilter(spell)

GetItemHealthFilter(item)

GetItemManaFilter(item)
	
GetHealthFilter(spell)

GetPetHealthFilter(spell)

GetUnitFilter(spell)

GetShardsFilter(spell)

GetCombopointFilter(spell)

GetWeaponFilter(spell)

DefineVendors(name, faction, execution)

DefineQuest(race, class, questId, questName, questType, pickupX, pickupY, pickupZ, pickupId, turninX, turninY, turninZ, turninId, questInfo, profileData, questRewards, isRepeatable)

ResetQuester()

LoadNextQuest()

DefineQuester(questerName, quests)

LoadQuester(questerName)

IsQuestCompleted(id)

IsQuestCompletable(id)

IsQuestActive(id)

AbandonQuest(questAbandonId)

DefineSettings(state, settings)

DefineSetting(state, setting)

SetQuestingState(x)

Questing.FollowNpc(id, distance)

Questing.InteractWith(x, y, z, id, dynamicFlag, distance, delay)

Questing.KillEnemy(x, y, z, id)

Questing.UseItemOnNpc(x, y, z, npcId, itemId, distance)

Questing.CastSpellOnNpc(x, y, z, npcId, spellId, distance)

Questing.EmoteAtNpc(x, y, z, npcId, emote, distance)

Questing.UseItemOnPosition(x, y, z, itemId, distance)

Questing.UseItemOnGround(x, y, z, itemId, distance)

Questing.ExtraActionButton1(x, y, z)

Questing.GossipWith(x, y, z, id, delay, distance, buttonIndex)

Questing.GetQuestInfo(questId)

Questing.IsObjectiveCompleted(questId, index)

Questing.GetObjectiveFulfilled(questId, index)

--------------------------------


GMR.IsObjectInteractable = function(object)

GMR.IsFullyLoaded = function()

GMR.GetWorldPositionFromMap = function(mapId, mapX, mapY)

GMR.GetDirectoryFiles = wmbapi.GetDirectoryFiles

GMR.ObjectPosition = function(object)

GMR.ObjectIsFacing = function(Object1, Object2)

GMR.ObjectId = function(object)

GMR.UnitMovementFlags = wmbapi.UnitMovementFlags

GMR.ObjectExists = function(object)

GMR.ObjectPointer = wmbapi.GetObject

GMR.ObjectName = UnitName

RunString(input, dest)

GMR.ReadFile = wmbapi.ReadFile

GMR.GetCameraPosition = wmbapi.GetCameraPosition

GMR.TraceLine = wmbapi.TraceLine

GMR.ClickPosition = wmbapi.ClickPosition

GMR.IsSpellPending = IsSpellTargeting

GMR.MoveTo = function(x, y, z, normal)

GMR.FaceDirection = function(x, y, z)

GMR.GetCorpsePosition = function()

GMR.GetObjectWithIndex = wmbapi.GetObjectWithIndex

GMR.ObjectCount = function()

GMR.ObjectRawType = function(obj)

GMR.ObjectRawFacing = wmbapi.ObjectFacing

GMR.ObjectCreatureTypeId = wmbapi.UnitCreatureTypeId

GMR.IsObjectSkinnable = function(object)

GMR.IsObjectLootable = wmbapi.UnitIsLootable

GMR.RunMacroText = RunMacroText

GMR.ObjectDynamicFlags = function(object)

GMR.ObjectFlags = wmbapi.UnitFlags

GMR.ObjectFlags2 = function(object)

GMR.ObjectAnimationFlag = wmbapi.ObjectAnimationState

GMR.GetWowMapId = function()

GMR.GetPath = function(destX, destY, destZ, isHardcoded)

GetFlightPath(startX, startY, startZ, destX, destY, destZ, adjust)

GMR.UpdateAFK = function()

GMR.Reload = function()

GMR.Logout = function()

GMR.Shutdown = function()

SendRequestAndThen(url, callback)

SendPostRequest(url, path, isHttps, body, headers, onSuccess)